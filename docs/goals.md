# Ideuxlius
## Successor to the terrible idalius IRC bot

## Introduction

Ideuxlius is a spiritual (emotional?) successor to the rough-as-guts idalius
IRC bot I wrote in Perl a few years ago. This bot was a dirty hobby project
serving a small channel of friends, and was thus lacking polish. While writing
it, I started having ideas for an IRC bot comprising a set of microservices
working together to form a modular, pluggable and even highly available IRC
bot.

## Goals

### Horizontal scaling and load balancing

This is less of a concern with IRC bots, which generally deal in low traffic
environments. However, you never know what sort of resource-intensive task
someone might write a module for, so the bot should support near-arbitrary
horizontal scaling at every part of the system. It also paves the way for high
availability.

### High availability: no single point of failure

We must support (but not demand) setups and topologies that insert no single
points of failure for the full operation of the bot. Modules need to remain
stateless where possible. Where this cannot be avoided, there needs to be a
process of reconciliation of state e.g. through leader elections or other forms
of mutual exclusion.

### Runtime configurability through runtime pluggability

If we have high availability, then we don't need to support runtime
configuration changes to an instance of a component. Instead of dealing with
extra code/logic to correctly handle on-the-fly configuration changes, we can
simply attach new instances of a component with our new configuration to the
bot, and start removing the old ones. Kubernetes folks call this a rolling
upgrade strategy.

This also encourages canary deployments of new software, and new configuration.

### Observability and tracing

If stepped/canary deployments are employed, it becomes advantageous to quantify
the appropriateness of the new version of software. Automated quantification in
the form of metrics gathered during normal operation of each component in the
system will help identify problems with a new rollout early in the piece,
reducing mean time to recovery. Useful logs are also obvious/mandatory.

As a system's components grow in number, figuring out where performance issues
and other bugs come from becomes more difficult than in simpler more monolithic
systems. As a mitigation, the ability to enable runtime tracing to the system
should be supported at a system-wide level to present the latency introduced by
each individual component as it is processed by the system.
