# Fair Workers

Code here is based on the RabbitMQ Go tutorial 2: https://www.rabbitmq.com/tutorials/tutorial-two-go.html

This PoC demonstrates using rabbitmq work queus with QoS in order to distribute
a stream of varied-size jobs to workers in a manner that illustrates the lowest
queue wait.

	                                   / worker1
	sender -> ... .. . ... ... .. .. -<  worker2
	                                   \ worker3

This topology would be suitable for distributing work from the bot core out to
replicas of each of the components. In order to run rabbitmq with three of the
workers attached, use the docker compose. Then use `./sender ...` to enqueue a
job that takes 3 seconds. Vary the number of dots to set jobs duration.

An example example scenario where this type of work might be useful in the bot:

In quick succession, 6 messages containing URLs are posted to channels the bot
is watching. These URLs are queued to have their HTML titles fetched so these
can be posted back to the respective channels.

We don't know it yet, but one of the URLs is an outlier, and fetching it will
take a long time, since the server at the URL is slow to respond. If we naively
assign the jobs round-robin to workers as they come into the queue, we might
end up with:

	          time (10 ms/col) -->
	worker 1: 11111111111111111111111111111111111111111111111111111111111144444
	worker 2: 22222222222255555555
	worker 3: 333333333666666666
	average queue wait: (T1 + T2 + T3)/6 = 135 ms

We see that Job 4 suffers delay unnecessarily because it was queued behind a
slow job on worker 1. But the other two workers were finished and idle long
before worker 1 was finished with even its first job. If we use QoS to limit
the number of jobs assigned to each worker at any one time to 1, the jobs wait
in the queue to be picked by the next ready worker.

	          time (10 ms/col) -->
	worker 1: 111111111111111111111111111111111111111111111111111111111111
	worker 2: 22222222222255555555
	worker 3: 33333333344444666666666
	average queue wait: (T2 + T3 + T3 + T4)/6 = 58.3 ms

In this case by enabling QoS with prefetch count of 1 we've halved the average
queue wait time of the jobs.
