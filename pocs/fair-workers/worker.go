package main

import (
	"log"
	"bytes"
	"time"
	"github.com/streadway/amqp"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer ch.Close()
	err = ch.Qos(
		1,
		0,
		false,
	)
	if err != nil {
		log.Fatalf(err.Error())
	}
	q, err := ch.QueueDeclare(
		"hello",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf(err.Error())
	}
	msgs, err := ch.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Println("Ready to consume")
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("Received: %s\n", d.Body)
			dotCount := bytes.Count(d.Body, []byte("."))
			time.Sleep(time.Duration(dotCount) * time.Second)
			log.Printf("Done\n")
			d.Ack(false)
		}
	}()
	<-forever
}
