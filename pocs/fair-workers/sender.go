package main
import (
	"os"
	"log"
	"github.com/streadway/amqp"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672")
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer conn.Close()
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf(err.Error())
	}
	body := os.Args[1]
	err = ch.Publish(
		"",
		q.Name,
		false,
		false,
		amqp.Publishing {
			DeliveryMode: amqp.Persistent,
			ContentType: "text/plain",
			Body: []byte(body),
		})
}
